﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace CenterRateService
{
    public partial class CenterRateBroadcast : ServiceBase
    {
        public CenterBroadCastRate objCBCRate; 
        public CenterRateBroadcast()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                System.Threading.Thread.Sleep(5000);
                objCBCRate = new CenterBroadCastRate();
            }
            catch (Exception ex)
            {
                clsMessage.setError("CenterRateBroadcast :: OnStart :: Error :: " + ex.Message + " :: " + ex.StackTrace);
            }
        }

        protected override void OnStop()
        {
            objCBCRate.Stop(true);
        }
    }
}
