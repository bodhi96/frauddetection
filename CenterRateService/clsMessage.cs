﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace CenterRateService
{
    public class clsMessage
    {

        public static string strServerName = "Data Source = 51.89.183.56,1533; Initial Catalog = CenterDB; User ID = user_CenterDB; Password=Center@6975;Connect Timeout = 30; Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public static SqlConnection con = new SqlConnection(strServerName);

        public static DateTime GetDatetime()
        {
            return DateTime.UtcNow;
        }

        public static void setError(string strDescription)
        {

            try
            {
                if (con.State != ConnectionState.Open)
                    con.Open();
                string strInsert = "INSERT INTO [dbo].[tblMsg]([appMessage],[appDateTime]) VALUES(@appMessage,@appDateTime) ";

                using (SqlCommand com = new SqlCommand(strInsert, con))
                {
                    com.CommandText = strInsert;
                    com.CommandType = System.Data.CommandType.Text;
                    com.Parameters.Clear();
                    com.Parameters.AddWithValue("@appMessage", "Window Service :: " + strDescription);
                    com.Parameters.AddWithValue("@appDateTime", GetDatetime());

                    int i = com.ExecuteNonQuery();

                }

            }
            catch (Exception ex)
            {
                setError("clsCustomeQuery :: setError :: Error :: " + ex.Message + " :: " + ex.StackTrace);
            }
            finally
            {
                con.Close();
            }
        }
    }
}
