USE [CenterDB]
GO
/****** Object:  StoredProcedure [dbo].[Check_CheatingBet]    Script Date: 10/30/2021 4:00:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[Check_CheatingBet] 
	 @appSiteID SMALLINT
	,@appBetID INT
	,@appBFCentralID DECIMAL(18,10)
	,@appSportId INT
	,@appBfSportID INT
	,@appTournamentID INT
	,@appBFTournamentID INT
	,@appMatchID INT
	,@appBFMatchID INT
	,@appMarketId INT
	,@appBFMarketID INT
	,@appSportName VARCHAR(240)
	,@appTournamentName VARCHAR(240)
	,@appMarketName VARCHAR(240)
	,@appMatchName VARCHAR(240)
	,@appMarketType VARCHAR(240)
	,@appRunnerID BIGINT
	,@appBFRunnerID BIGINT
	,@appRunnerName VARCHAR(240)
	,@appBetDate DATETIME
	,@appIsBack BIT
	,@appRate DECIMAL(18, 8)
	,@appStakeAmount DECIMAL(18, 2)
	,@appTimeVariationUpper TINYINT = 0
	,@appTimeVariationLower TINYINT = 0
	,@appCheckBestRate TINYINT
AS
SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

BEGIN
		
	BEGIN TRY

		IF(@appTimeVariationUpper = 0)
		BEGIN
			SET @appTimeVariationUpper = 10;
		END
		IF(@appTimeVariationLower = 0)
		BEGIN
			SET @appTimeVariationLower = 10;
		END


		DECLARE @startdate DATETIME = DATEADD(SECOND, - @appTimeVariationUpper, @appBetDate)
		SET @startdate = CONVERT(DATETIME, CONVERT(DATETIME, @startdate, 120), 103)

		DECLARE @enddate DATETIME = DATEADD(SECOND, @appTimeVariationLower, @appBetDate)
		SET @enddate = CONVERT(DATETIME, CONVERT(DATETIME, @enddate, 120), 103)
		
		
		DECLARE @appAllowedVariationLimit DECIMAL(18, 2);
		SET  @appAllowedVariationLimit = ( SELECT dbo.udf_GetAllowedVariationLimit(@appRate)  as 'appAllowedVariationLimit')

		DROP TABLE IF EXISTS #lstRate

		SELECT jsonRate,createdDate
		INTO #lstRate
		FROM tblRate
		WHERE centerid = @appBFCentralID
		AND (createdDate BETWEEN @startdate AND @enddate)

		DROP TABLE IF EXISTS #tempRate

		CREATE TABLE #tempRate( centerID VARCHAR(16)
								,runnerID BIGINT
								,bdatb VARCHAR(160)
								,bdatl VARCHAR(160)
								,createdDate datetime
		)

		INSERT INTO #tempRate
		SELECT centerID, runnerID, bdatl, bdatb , lst.createdDate
		FROM #lstRate lst 
		CROSS APPLY (
			SELECT a.id AS centerID
				,rate.id AS runnerID
				,rate.bdatl as bdatl
				,rate.bdatb as bdatb
			FROM OPENJSON(lst.jsonRate) WITH (
					id VARCHAR(16)
					,marketDefinition NVARCHAR(max) AS json
					,rc NVARCHAR(max) AS json
			) AS a
			CROSS APPLY OPENJSON(a.rc) WITH (
					bdatl NVARCHAR(MAX) AS JSON
					,bdatb NVARCHAR(MAX) AS JSON
					,id INT
			) AS rate
			WHERE a.rc is not null  and rate.id = @appBFRunnerID
		) x 
		WHERE (x.bdatl is not null or x.bdatb is not null )

		DROP TABLE IF EXISTS #tempRate_Log
		
		CREATE TABLE #tempRate_Log ( 
				centerID decimal(18,10), 
				runnerID bigint, 
				appIsBack bit, 
				createdDate datetime,
				appRank tinyInt, 
				appVariation decimal(8,2) 
		)
		
		IF EXISTS ( SELECT 1 FROM #tempRate)
		BEGIN
			IF(@appIsBack = 0)
			BEGIN 
				INSERT INTO #tempRate_Log
				SELECT lstRates.centerID
					,lstRates.runnerID 
					,lstRates.appIsBack
					,lstRates.createdDate
					,frank.rateRank AS appRank
				,CASE WHEN @appRate  > frate.rate
						THEN @appRate - frate.rate
						ELSE frate.rate - @appRate	
				END AS appVariation
				FROM (
					SELECT centerID
							,runnerID
							,0 AS appIsBack
							,result
							,createdDate
					FROM #tempRate lst
					CROSS APPLY (
						SELECT [VALUE] AS result 
						FROM OPENJSON(lst.bdatl) AS JSON
					) AS lay
					WHERE result IS NOT NULL
				)  lstRates
				CROSS APPLY (
					SELECT [VALUE] as rate FROM openJson(lstRates.result) where [key] = 1
				) frate
				CROSS APPLY (
					SELECT [VALUE] as rateRank FROM openJson(lstRates.result) where [key] = 0
				) frank
				WHERE frate.rate > 1.0
			END
			IF(@appIsBack = 1)
			BEGIN 
				INSERT INTO #tempRate_Log
				SELECT lstRates.centerID
					,lstRates.runnerID 
					,lstRates.appIsBack
					,lstRates.createdDate
					,frank.rateRank AS appRank
				,CASE WHEN @appRate  > frate.rate
						THEN @appRate - frate.rate
						ELSE frate.rate - @appRate	
				END AS appVariation
				FROM (
						SELECT centerID
							,runnerID
							,1 AS appIsBack
							,result
							,createdDate
						FROM #tempRate lst
						CROSS APPLY (
							SELECT [VALUE] AS result
							FROM OPENJSON(lst.bdatb) AS JSON
						) AS back
						WHERE result IS NOT NULL
				)  lstRates
				CROSS APPLY (
					SELECT [VALUE] as rate FROM openJson(lstRates.result) where [key] = 1
				) frate
				CROSS APPLY (
					SELECT [VALUE] as rateRank FROM openJson(lstRates.result) where [key] = 0
				) frank
				WHERE frate.rate > 1.0
			END
		END


		DECLARE @maxVariationFound DECIMAL(18, 2) = 0.00;
		DECLARE @beforeVariationFound DECIMAL(18, 2) = 0.00;
		DECLARE @afterVariationFound DECIMAL(18, 2) = 0.00;

		IF (@appCheckBestRate = 0)
		BEGIN
			SELECT @maxVariationFound = Max(RESULT.appVariation)
			FROM #tempRate_Log AS RESULT
			WHERE appIsBack = @appIsBack

			SELECT @beforeVariationFound = Max(RESULT.appVariation)
			FROM #tempRate_Log AS RESULT
			WHERE appIsBack = @appIsBack
				AND RESULT.createdDate < @appBetDate

			SELECT @afterVariationFound = Max(RESULT.appVariation)
			FROM #tempRate_Log AS RESULT
			WHERE appIsBack = @appIsBack
				AND RESULT.createdDate >= @appBetDate
		END
		ELSE
		BEGIN
			SELECT @maxVariationFound = Max(RESULT.appVariation)
			FROM #tempRate_Log AS RESULT
			WHERE appIsBack = @appIsBack
				AND RESULT.appRank = @appCheckBestRate

			SELECT @beforeVariationFound = Max(RESULT.appVariation)
			FROM #tempRate_Log AS RESULT
			WHERE appIsBack = @appIsBack
				AND RESULT.appRank = @appCheckBestRate
				AND RESULT.createdDate < @appBetDate

			SELECT @afterVariationFound = Max(RESULT.appVariation)
			FROM #tempRate_Log AS RESULT
			WHERE appIsBack = @appIsBack
				AND RESULT.appRank = @appCheckBestRate
				AND RESULT.createdDate >= @appBetDate
		END

	
		DECLARE @appIsValid BIT = 0;
		DECLARE @appValidType TINYINT = 0;

		IF (@afterVariationFound > @appAllowedVariationLimit AND @beforeVariationFound > @appAllowedVariationLimit)
			BEGIN
--			SELECT 'BET IS INVALID';
			SET @appIsValid = 0;
			SET @appValidType = 2;
		END
		ELSE IF (@afterVariationFound > @appAllowedVariationLimit OR @beforeVariationFound > @appAllowedVariationLimit)
			BEGIN
	--			SELECT 'BET IS DOUBTFULL';
				SET @appIsValid = 0;
				SET @appValidType = 1;
			END
		ELSE
			BEGIN
			SET @appIsValid = 1;
			SET @appValidType = 0;
--			SELECT 'BET IS VALID';
		END

		IF(@appIsValid = 0)
			BEGIN
				INSERT INTO [ReportingServiceLive].[dbo].[tblCheatBet]
						   ([appSiteID]
						   ,[appBetID]
						   ,[appBFCentralID]
						   ,[appSportId]
						   ,[appBFSportID]
						   ,[appTournamentID]
						   ,[appBFTournamentID]
						   ,[appMatchID]
						   ,[appBFMatchID]
						   ,[appMarketId]
						   ,[appBFMarketID]
						   ,[appMarketType]
						   ,[appSportName]
						   ,[appTournamentName]
						   ,[appMarketName]
						   ,[appMatchName]
						   ,[appRunnerID]
						   ,[appBFRunnerID]
						   ,[appRunnerName]
						   ,[appBetDate]
						   ,[appIsBack]
						   ,[appRate]
						   ,[appStakeAmount]
						   ,[appTimeVariationUpper]
						   ,[appTimeVariationLower]
						   ,[appCheckBestRate]
						   ,[appAllowedVariationLimit]
						   ,[appMaxVariationFound]
						   ,[appBeforeVariationFound]
						   ,[appAfterVariationFound]
						   ,[appIsValid]
						   ,[appValidType])
					 VALUES
						   (@appSiteID
						   ,@appBetID
						   ,@appBFCentralID
						   ,@appSportId
						   ,@appBFSportID
						   ,@appTournamentID
						   ,@appBFTournamentID
						   ,@appMatchID
						   ,@appBFMatchID
						   ,@appMarketId
						   ,@appBFMarketID
						   ,@appMarketType
						   ,@appSportName
						   ,@appTournamentName
						   ,@appMarketName
						   ,@appMatchName
						   ,@appRunnerID
						   ,@appBFRunnerID
						   ,@appRunnerName
						   ,@appBetDate
						   ,@appIsBack
						   ,@appRate
						   ,@appStakeAmount
						   ,@appTimeVariationUpper
						   ,@appTimeVariationLower
						   ,@appCheckBestRate
						   ,@appAllowedVariationLimit
						   ,@maxVariationFound
						   ,@beforeVariationFound
						   ,@afterVariationFound
						   ,@appIsValid
						   ,@appValidType)
			
			END		

		DROP TABLE IF EXISTS #lstRate
		DROP TABLE IF EXISTS #tempRate
		DROP TABLE IF EXISTS #tempRate_Log

	END TRY
	BEGIN CATCH
		DROP TABLE IF EXISTS #lstRate
		DROP TABLE IF EXISTS #tempRate
		DROP TABLE IF EXISTS #tempRate_Log
	END CATCH

END
