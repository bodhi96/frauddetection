﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using System.Net.WebSockets;
using System.Threading;
using System.IO;
using System.Data.SqlClient;
using Microsoft.AspNetCore.SignalR.Client;
using System.Collections.Concurrent;
using Dapper;
using Newtonsoft.Json;

namespace CenterRateService
{

    public class CenterBroadCastRate
    {
        public static string conRepoStr = "Data Source=51.89.183.56,1533;User ID=user_reportingService_Live;Password=India@123;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public static string conStr = "Data Source=51.89.183.56,1533;Initial Catalog=CenterDB;User ID=user_CenterDB;Password=Center@6975;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        public static string conWebSocket = "wss://central2.satsport248.com:8888/?token=WS_L4S_RepoServer_tesing";
        public static string conWebSocketAlpha = "wss://central3.satsport248.com:8881/?token=WS_L4S_RepoServer_tesing";
        public static SqlConnection con = new SqlConnection(conStr);

        private static ConcurrentDictionary<int, string> lstMarkets = new ConcurrentDictionary<int, string>();
        public static List<HubPlatform> lstHubs;

        public void Stop(bool v)
        {
            try
            {
                clsMessage.setError("Stopping Service ::::::::::::::::::::::::::  ");

            }
            catch (Exception ex)
            {
                clsMessage.setError("Stop :: Removing All Markets :: " + ex.Message + " :: " + ex.StackTrace);
            }
        }

        public CenterBroadCastRate()
        {
            _ = InitSocketAsync();
        }

        public async Task InitSocketAsync()
        {
            clsMessage.setError("Initiating Service ");

            using (IDbConnection db = new SqlConnection(conRepoStr))
            {
                lstHubs = db.Query<HubPlatform>("USP_GetHubPlatformList", commandType: CommandType.StoredProcedure).AsList();
                if (lstHubs != null && lstHubs.Count > 0)
                {
                    foreach (HubPlatform hub in lstHubs)
                    {
                        if (hub.SignalrUrl.ToUpper().Contains("ALPHA"))
                            await ConnectAlphaHubAsync(hub);
                        else
                            await ConnectHubAsync(hub);
                    }
                }
            }
        }


        public async Task ConnectHubAsync(HubPlatform hubPlatform)
        {
            //lstMessage.Items.Add(" Registering Market : " + hubPlatform.market_id + " for Sports  : " + hubPlatform.sport_id + " in Tournament :" + sbr.tournament_id + " & event : " + sbr.event_id);
            try
            {
                Microsoft.AspNetCore.SignalR.Client.HubConnection connection = new Microsoft.AspNetCore.SignalR.Client.HubConnectionBuilder()
                  .WithUrl(hubPlatform.SignalrUrl)
                 .WithAutomaticReconnect()
                 .Build();

                connection.Closed += async (error) =>
                {
                    //lstMessage.Items.Add(hubPlatform.SignalrUrl + " Connection Disconnected ");
                    await connection.InvokeAsync("LeaveAdmin", 1);
                    await Task.Delay(new Random().Next(0, 5) * 1000);
                    await connection.StartAsync();
                };

                connection.On<int, int, Boolean, int>(hubPlatform.SignalrMethod, async (int BetId, int MarketId, bool IsUnMatched, int ChangeType) =>
                {
                    //lstMessage.Items.Add(hubPlatform.SignalrUrl + " -> Recieved  Bet : " + BetId + " in Market : " + MarketId);
                    clsMessage.setError("Connecting Hub : " + hubPlatform.SignalrUrl);
                    if (!lstMarkets.ContainsKey(MarketId))
                    {
                        //DataTable betDetail = new DataTable();
                        MktRequest mkt = new MktRequest();
                        mkt.data = new List<SubRequest>();
                        SubRequest sbr = new SubRequest();
                        SubResponse res = new SubResponse();
                        try
                        {
                            using (IDbConnection db = new SqlConnection(conRepoStr))
                            {
                                var queryParameters = new DynamicParameters();
                                queryParameters.Add("@HubUrl", hubPlatform.SignalrUrl.ToString());
                                queryParameters.Add("@BetID", BetId.ToString());
                                queryParameters.Add("@MarketID", MarketId.ToString());
                                res = db.QuerySingleOrDefault<SubResponse>("USP_GetMarketDetailByHub", queryParameters, commandType: CommandType.StoredProcedure);
                                if (res != null)
                                {
                                    sbr.event_id = res.BFMatchID.ToString();
                                    sbr.market_id = res.BFMarketID.ToString();
                                    sbr.sport_id = res.BfSportID.ToString();
                                    sbr.tournament_id = res.TournamentID.ToString();
                                    mkt.data.Add(sbr);
                                    clsMessage.setError("Registering  Market : " + sbr.market_id);
                                    //lstMessage.Items.Add(" Registering Market : " + sbr.market_id + " for Sports  : " + sbr.sport_id + " in Tournament :" + sbr.tournament_id + " & event : " + sbr.event_id);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //lstMessage.Items.Add("Error : " + ex.Message);
                            clsMessage.setError("Error : " + ex.Message);
                        }
                        if (mkt.data.Count > 0 && sbr.market_id != null)
                        {
                            lstMarkets.AddOrUpdate(MarketId, sbr.market_id, (key, newvalue) => newvalue);
                            mkt.action = "betfair_to_central_bf_res";

                            await WebSocketStart(mkt, conWebSocket);
                        }
                    }
                    // check for cheating bet here 
                    BetResponse betRes = new BetResponse();

                    using (IDbConnection db = new SqlConnection(conRepoStr))
                    {
                        // DataTable betDetail = new DataTable();
                        MktRequest mkt = new MktRequest();
                        SubRequest sbr = new SubRequest();


                        var queryParameters = new DynamicParameters();
                        queryParameters.Add("@HubUrl", hubPlatform.SignalrUrl.ToString());
                        queryParameters.Add("@BetID", BetId.ToString());
                        queryParameters.Add("@MarketID", MarketId.ToString());
                        betRes = db.QuerySingleOrDefault<BetResponse>("USP_GetBetDetailByHub", queryParameters, commandType: CommandType.StoredProcedure);
                    }
                    if (betRes != null)
                    {
                        string query = "Check_CheatingBet";         //Stored Procedure name  
                        if (con.State != ConnectionState.Open)
                            con.Open();

                        using (SqlCommand com = new SqlCommand(query, con))
                        {
                            com.CommandType = CommandType.StoredProcedure;  //here we declaring command type as stored Procedure  
                            com.Parameters.AddWithValue("@appSiteID", hubPlatform.SiteID);
                            com.Parameters.AddWithValue("@appBetID", betRes.BetID);        //first Name  
                            com.Parameters.AddWithValue("@appBFCentralID", betRes.BFMarketID);
                            com.Parameters.AddWithValue("@appSportId", betRes.SportId);
                            com.Parameters.AddWithValue("@appBfSportID", betRes.BFSportId);
                            com.Parameters.AddWithValue("@appTournamentID", betRes.TournamentID);
                            com.Parameters.AddWithValue("@appBFTournamentID", betRes.BFTournamentID);
                            com.Parameters.AddWithValue("@appMatchID", betRes.MatchID);
                            com.Parameters.AddWithValue("@appBFMatchID", betRes.BFMatchID);
                            com.Parameters.AddWithValue("@appMarketId", betRes.MarketID);
                            com.Parameters.AddWithValue("@appBFMarketID", betRes.BFMarketID);
                            com.Parameters.AddWithValue("@appSportName", betRes.SportName);
                            com.Parameters.AddWithValue("@appTournamentName", betRes.TournamentName);
                            com.Parameters.AddWithValue("@appMarketName", betRes.MarketName);
                            com.Parameters.AddWithValue("@appMatchName", betRes.MatchName);
                            com.Parameters.AddWithValue("@appMarketType", betRes.MarketType);
                            com.Parameters.AddWithValue("@appRunnerID", betRes.RunnerID);
                            com.Parameters.AddWithValue("@appBFRunnerID", betRes.BFRunnerID);
                            com.Parameters.AddWithValue("@appRunnerName", betRes.RunnerName);
                            com.Parameters.AddWithValue("@appIsBack", betRes.IsBack);
                            com.Parameters.AddWithValue("@appRate", betRes.Rate);
                            com.Parameters.AddWithValue("@appStakeAmount", betRes.Stake);
                            com.Parameters.AddWithValue("@appBetDate", betRes.CreatedDate);
                            com.Parameters.AddWithValue("@appTimeVariationUpper", 0);
                            com.Parameters.AddWithValue("@appTimeVariationLower", 0);
                            com.Parameters.AddWithValue("@appCheckBestRate", 0);

                            com.ExecuteNonQuery();
                        }
                    }
                });
                connection.StartAsync();
                connection.InvokeAsync("JoinAdmin", 1);

               
            }
            catch (Exception ex)
            {
                clsMessage.setError("ConnectHubAsync Error : " + ex.Message);
            }
        }

        public async Task ConnectAlphaHubAsync(HubPlatform hubPlatform)
        {

            //lstMessage.Items.Add(" Registering Market : " + hubPlatform.market_id + " for Sports  : " + hubPlatform.sport_id + " in Tournament :" + sbr.tournament_id + " & event : " + sbr.event_id);
            try
            {
                Microsoft.AspNetCore.SignalR.Client.HubConnection connection = new Microsoft.AspNetCore.SignalR.Client.HubConnectionBuilder()
                  .WithUrl(hubPlatform.SignalrUrl)
                 .WithAutomaticReconnect()
                 .Build();
                clsMessage.setError("Connecting hub: " + hubPlatform.SignalrUrl);

                connection.Closed += async (error) =>
                {
                    clsMessage.setError("Closing hub");

                    //lstMessage.Items.Add(hubPlatform.SignalrUrl + " Connection Disconnected ");
                    await connection.InvokeAsync("LeaveAdmin", 1);
                    await Task.Delay(new Random().Next(1, 5) * 1000);
                    await connection.StartAsync();
                };

                connection.On<SendAdminBetChangeDetailModel>(hubPlatform.SignalrMethod, async (SendAdminBetChangeDetailModel betDet) =>
                {
                    clsMessage.setError("Received bet on: " + hubPlatform.SignalrUrl + " --- " + betDet.betId + " for mkt " + betDet.bfMarketId);
                    if (!lstMarkets.ContainsKey(betDet.marketId))
                    {
                        lstMarkets.AddOrUpdate(betDet.marketId, betDet.bfMarketId, (key, newvalue) => newvalue);

                        clsMessage.setError("Connecting Hub : " + hubPlatform.SignalrUrl);
                        MktRequest mkt = new MktRequest();
                        mkt.data = new List<SubRequest>();
                        SubRequest sbr = new SubRequest();
                        sbr.event_id = betDet.bfMatchId.ToString();
                        sbr.market_id = betDet.bfMarketId;
                        sbr.sport_id = betDet.bfSportId.ToString();
                        sbr.tournament_id = betDet.bfTournamentId.ToString();
                        mkt.data.Add(sbr);

                        mkt.action = "betfair_to_central_bf_res";

                        await WebSocketStart(mkt, conWebSocketAlpha);
                    }

                    string query = "Check_CheatingBet";         //Stored Procedure name  
                    if (con.State != ConnectionState.Open)
                        con.Open();

                    using (SqlCommand com = new SqlCommand(query, con))
                    {
                        com.CommandType = CommandType.StoredProcedure;  //here we declaring command type as stored Procedure  
                        com.Parameters.AddWithValue("@appSiteID", hubPlatform.SiteID);
                        com.Parameters.AddWithValue("@appBetID", betDet.betId);        //first Name  
                        com.Parameters.AddWithValue("@appBFCentralID", betDet.bfMarketId);
                        com.Parameters.AddWithValue("@appSportId", betDet.sportId);
                        com.Parameters.AddWithValue("@appBfSportID", betDet.bfSportId);
                        com.Parameters.AddWithValue("@appTournamentID", betDet.tournamentId);
                        com.Parameters.AddWithValue("@appBFTournamentID", betDet.bfTournamentId);
                        com.Parameters.AddWithValue("@appMatchID", betDet.matchId);
                        com.Parameters.AddWithValue("@appBFMatchID", betDet.bfMatchId);
                        com.Parameters.AddWithValue("@appMarketId", betDet.marketId);
                        com.Parameters.AddWithValue("@appBFMarketID", betDet.bfMarketId);
                        com.Parameters.AddWithValue("@appSportName", betDet.sportName);
                        com.Parameters.AddWithValue("@appTournamentName", ""); // betRes.TournamentName);
                        com.Parameters.AddWithValue("@appMarketName", betDet.marketName);
                        com.Parameters.AddWithValue("@appMatchName", betDet.matchName);
                        com.Parameters.AddWithValue("@appMarketType", betDet.marketType);
                        com.Parameters.AddWithValue("@appRunnerID", ""); // betRes.RunnerID);
                        com.Parameters.AddWithValue("@appBFRunnerID", betDet.bfRunnerId);
                        com.Parameters.AddWithValue("@appRunnerName", betDet.runnerName);
                        com.Parameters.AddWithValue("@appIsBack", betDet.isBack);
                        com.Parameters.AddWithValue("@appRate", betDet.rate);
                        com.Parameters.AddWithValue("@appStakeAmount", betDet.stake);
                        com.Parameters.AddWithValue("@appBetDate", DateTime.Now);
                        com.Parameters.AddWithValue("@appTimeVariationUpper", 0);
                        com.Parameters.AddWithValue("@appTimeVariationLower", 0);
                        com.Parameters.AddWithValue("@appCheckBestRate", 0);

                        com.ExecuteNonQuery();
                    }
                });
                connection.StartAsync();
                connection.InvokeAsync("JoinAdmin", 1);
            }
            catch (Exception ex)
            {
                clsMessage.setError("ConnectAlphaHubAsync Error : " + ex.Message);
            }
        }

        static async Task WebSocketStart(MktRequest mkt, string socketUrl)
        {
            using (var socket = new ClientWebSocket())
            {
                try
                {
                    await socket.ConnectAsync(new Uri(socketUrl), CancellationToken.None);
                    await WebSocketSend(socket, mkt);
                    await WebSocketReceive(socket);
                }
                catch (Exception ex)
                {
                    clsMessage.setError("WebSocketStart Error : " + ex.Message);
                }
            }
        }
        static async Task WebSocketSend(ClientWebSocket socket, MktRequest mkt)
        {
            var jsonObj = Newtonsoft.Json.JsonConvert.SerializeObject(mkt);
            System.ArraySegment<byte> buffer = new ArraySegment<byte>(Encoding.UTF8.GetBytes(jsonObj));

            await socket.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
        }
        static async Task WebSocketReceive(ClientWebSocket socket)
        {
            var buffer = new ArraySegment<byte>(new byte[2048]);
            do
            {
                WebSocketReceiveResult result;
                using (var ms = new MemoryStream())
                {
                    do
                    {
                        result = await socket.ReceiveAsync(buffer, CancellationToken.None);
                        ms.Write(buffer.Array, buffer.Offset, result.Count);
                    } while (!result.EndOfMessage);
                    ms.Seek(0, SeekOrigin.Begin);

                    using (var reader = new StreamReader(ms, Encoding.UTF8))
                    {
                        //DataTable finalDt = new DataTable();

                        try
                        {
                            string data = await reader.ReadToEndAsync();
                            if (!string.IsNullOrEmpty(data) && !string.IsNullOrWhiteSpace(data))
                            {
                                // lstMsg.Items.Add("Data Received " + data);
                                string query = "AddRate";         //Stored Procedure name  
                                JsonObj jobj = JsonConvert.DeserializeObject<JsonObj>(data);
                                if (con.State != System.Data.ConnectionState.Open)
                                    con.Open();
                                using (SqlCommand com = new SqlCommand(query, con))
                                {
                                    com.CommandType = CommandType.StoredProcedure;  //here we declaring command type as stored Procedure  
                                    com.Parameters.AddWithValue("@CenterID", jobj.id);        //first Name  
                                    com.Parameters.AddWithValue("@Rate", "[" + data + "]");
                                    com.ExecuteNonQuery();
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            clsMessage.setError("WebSocketReceive Error : " + e.Message);
                        }
                    }
                }

            } while (true);

        }

    }

}
