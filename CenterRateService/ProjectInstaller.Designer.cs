﻿namespace CenterRateService
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CenterRateServiceProceessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.CenterRateServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // CenterRateServiceProceessInstaller
            // 
            this.CenterRateServiceProceessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.CenterRateServiceProceessInstaller.Password = null;
            this.CenterRateServiceProceessInstaller.Username = null;
            // 
            // CenterRateServiceInstaller
            // 
            this.CenterRateServiceInstaller.DelayedAutoStart = true;
            this.CenterRateServiceInstaller.Description = "Demo";
            this.CenterRateServiceInstaller.DisplayName = "CenterRateBroadcast";
            this.CenterRateServiceInstaller.ServiceName = "CenterRateBroadcast";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.CenterRateServiceProceessInstaller,
            this.CenterRateServiceInstaller});

        }

        #endregion
        public System.ServiceProcess.ServiceProcessInstaller CenterRateServiceProceessInstaller;
        public System.ServiceProcess.ServiceInstaller CenterRateServiceInstaller;
    }
}