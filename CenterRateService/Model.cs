﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CenterRateService
{

    public class Model
    {
    }

    public class HubPlatform
    {
        public int HubID { get; set; }
        public int SiteID { get; set; }
        public string SignalrUrl { get; set; }
        public string SignalrMethod { get; set; }
    }
    public class JsonObj
    {
        [JsonProperty("id")]
        public Double id { get; set; }

    }
    public class MktRequest
    {
        public string action { get; set; }
        public List<SubRequest> data { get; set; }
    }
    public class SubResponse
    {
        public Double BFMarketID { get; set; }
        public int BFMatchID { get; set; }
        public int CentralID { get; set; }

        public int TournamentID { get; set; }
        public int SportId { get; set; }
        public int BfSportID { get; set; }
    }
    public class BetResponse
    {
        public int SportId { get; set; }

        public Double BFSportId { get; set; }
        public string SportName { get; set; }
        public int TournamentID { get; set; }
        public double BFTournamentID { get; set; }
        public string TournamentName { get; set; }
        public int MarketID { get; set; }
        public double BFMarketID { get; set; }

        public string MarketName { get; set; }
        public int MatchID { get; set; }
        public double BFMatchID { get; set; }
        public string MatchName { get; set; }

        public string MarketType { get; set; }
        public int RunnerID { get; set; }
        public double BFRunnerID { get; set; }
        public string RunnerName { get; set; }
        public int BetID { get; set; }
        public bool IsBack { get; set; }
        public float Rate { get; set; }
        public float Stake { get; set; }
        public int PlayerID { get; set; }

        public DateTime CreatedDate { get; set; }
    }
    public enum ChangeType
    {
        Add = 1,
        Remove = 2,
        Update = 3
    }
    public class SendAdminBetChangeDetailModel
    {
        public int adminId { get; set; }
        public int betId { get; set; }
        public int marketId { get; set; }
        public bool isUnmatchBet { get; set; }
        public string bfMarketId { get; set; }
        public string marketName { get; set; }
        public string marketType { get; set; }
        public int matchId { get; set; }
        public int bfMatchId { get; set; }
        public string matchName { get; set; }
        public int centralId { get; set; }
        public int tournamentId { get; set; }
        public int bfTournamentId { get; set; }
        public int sportId { get; set; }
        public int bfSportId { get; set; }
        public string sportName { get; set; }
        public int bfRunnerId { get; set; }
        public string runnerName { get; set; }
        public bool isBack { get; set; }
        public decimal rate { get; set; }
        public int stake { get; set; }
        public decimal? run { get; set; }
        public DateTime placeBetDate { get; set; }
        public int playerId { get; set; }
        public string playerName { get; set; }
        public string betInfo { get; set; }
        public ChangeType changeType { get; set; }
    }
    public class SubRequest
    {
        public string sport_id { get; set; }
        public string tournament_id { get; set; }

        public string event_id { get; set; }
        public string market_id { get; set; }
    }
}
